ftpbackup role to configure a ftp backup
----------------------------------------

These playbooks require Ansible 1.2.

This Ansible role installs a script to backup your data to a FTP space,
usually offered by your provider.

Required parameters:
- ftp url
- ftp user
- ftp password

By default, both files and MySQL data are backed up. This behaviour can be
modified in group_vars
The needed packages and files are installed during the playbook execution.

Required packages:
- mysql-client
- lftp
- backupchecker
